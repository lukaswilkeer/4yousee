/* eslint fp/no-unused-expression: "off" */

import express from 'express'
import expressVue from 'express-vue'
import path from 'path'
import { createServer } from 'http'
import { vueOptions } from './helpers/vueOptions'
import { router as galleryRouter } from './modules/gallery'

const app = express()

const views = path.join(__dirname, './views')

const expressVueMiddleware = expressVue.init(vueOptions(views))
app.use(expressVueMiddleware)
app.use('/static', express.static('static'))
app.use('/', galleryRouter)

const server = createServer(app).listen(9080)