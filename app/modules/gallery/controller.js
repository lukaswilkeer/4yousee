import axios from 'axios'
import { apiMock } from './mock'

const apiEndpoint = 'https://private-7cf60-4youseesocialtest.apiary-mock.com'

const handleError = (err) => {
  return { status: 500, msg: err }
}

export const getTimeline = () => {
  return axios.get(`${apiEndpoint}/timeline`)
    .then(result => {
      return result
    })
    .catch(handleError)
}

/*
  Temporary workaroud for timeout request from API.
  This demonstrate the flexibility of the stack.
*/
export const getTimelineMock = () => {
  const promise = new Promise((resolve, reject) => {
    return setTimeout(resolve, 100, apiMock())
  })

  return promise
}
