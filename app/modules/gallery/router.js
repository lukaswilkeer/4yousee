import Router from 'express'
import { getTimeline, getTimelineMock } from './controller'

const router = Router()

const debug = (result) => {
  return result
}

const render = (res) => (layout) => (data) => {
  // You can set custom vueOptions here
  return res.renderVue(layout, data)
}

const viewGallery = (req, res) => {
  const data = {
    msg: 'Olá mundo'
  }

  // Fixme: usar getTimeline()
  return getTimeline()
    .then((timeline) => Object.assign({}, data, { 'timeline': timeline.data }))
    .then(render(res)('gallery.vue'))
    .catch((err) => res.status(500).json({err : err }))
}

router.get('/', viewGallery)

export { router }