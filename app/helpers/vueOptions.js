
export const vueOptions = (views) => ({
  rootPath: views,
  layout: {
    start: '<div id="app">',
    end: '</div>'
  },
  vue: {
      head: {
      title: "4yousee teste desenvolvedor",
      meta: [
        { script: "https://unpkg.com/vue@2.4.4/dist/vue.js" },
        { script: "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.3/js/materialize.min.js" },
        { script: "/static/js/init.js" },
        { style: "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.3/css/materialize.min.css" },
        { style: "https://fonts.googleapis.com/icon?family=Material+Icons", rel: "stylesheet" }
      ]
    }
  }
})