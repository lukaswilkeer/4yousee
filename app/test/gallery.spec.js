import axios from 'axios'
import { expect } from 'chai'
import * as controller from '../modules/gallery/controller'

// fixme: variavel de acordo com o ambiente
const url = 'http://localhost:9080'

describe('Módulo de galeria', () => {
  it('deve estar instalado', done => {
    axios.get(`${url}/`).then((result) => {
      expect(result.status).to.be.eql(200)
      done()
    }).catch((err) => done(err))
  })
})

describe('Integração com a api 4yousee', () => {
  it('deve pegar as imagens', (done) => {
    const test = (response) => {
      const timeline = response.data

      expect(response.status).to.be.eql(200)
      expect(typeof(timeline)).to.be.eql('object')
      expect(typeof(timeline[0])).to.have.eql('object')
      expect(timeline[0]).to.be.have.property('type')
      expect(timeline[0]).to.be.have.property('category')  
      done()
    }
    
    controller.getTimeline()
      .then(test)
      .catch((err) => done(err))
  })

  it('mock da api', (done) => {
    const test = (response) => {
      const timeline = response.data

      expect(typeof (timeline)).to.be.eql('object')
      expect(typeof (timeline[0])).to.have.eql('object')
      expect(timeline[0]).to.be.have.property('type')
      expect(timeline[0]).to.be.have.property('category')
      done()
    }

    controller.getTimelineMock()
      .then(test)
      .catch((err) => done(err))
  })
})