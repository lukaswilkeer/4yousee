import axios from 'axios'
import { expect } from 'chai'

// fixme: variavel de acordo com o ambiente
const url = 'http://localhost:9080'

describe('Start da aplicação', () => {
  it('deve estar rodando', (done) => {
    axios.get(`${url}/`).then((result) => {
      expect(result.status).to.be.eql(200)
      done()
    }).catch((err) => done(err))
  })
})