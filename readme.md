# 4YouSee - Teste de desenvolvimento.

Feed de notícias desenvolvido em Node.js, Express e Vue.

## Stack e ferramentas

* Express
* Vue (SSR)
* Babel
* ESLint (Ver configurações)
* NW.js
* Materialize.css

## Estrutura
```
4yousee
  |-- app
  |   |-- helpers
  |   |-- modules
  |       |-- module
  |           |-- model.js
  |           |-- controller.js
  |           |-- router.js
  |   |-- test
  |   |-- views
  |       |-- components
  |-- build
  |-- dist
  |-- static
```
## Desenvolvimento

**Instalando dependências**

`npm install`

**Testando a aplicaçaõ**

`npm run test` - para realizar testes.

`npm run dev` - para rodar a aplicação em modo de desenvolvimento em `localhost:9080`.

`npm run dev.debug` -  o mesmo do anterior, mas habilita a depuração remota e escuta o Express.

`npm run lint` - Eslint

**Build e start**

`npm run dist` - compila para várias plataformas (ver `package.json`)

`npm run dist.linux` - build para x64 linux.

`npm run start` - roda aplicação no NW.js porém sem gerar build

## Processo para gerar uma nova build

Na sequência:

  * `npm run prepare` - Transpila, minifica arquivos.
  * `npm run dist` - NW.js build

## FAQ

**Como rodar um build**

Na pasta do aplicativo (no diretório `dist/`):

`chmod +x 4yousee`
`./4yousee`

TODO: Criar executavél e empacotamento do aplicativo

**Como adicionar um módulo?**

**Como declarar uma rota?**

**Como usar middleware?**


## Referências

**todo.minimal**: Arquivo contendo mudanças, issues, débitos técnicos e histórico.

